from tkinter import *

class ArchiveList(Tk):
    app = None

    def __init__(self, app, master = None):
        self.app = app
        Tk.__init__(self, master)
        Tk.title(self, app.getTitle() + ' - Archives')
        Tk.geometry(self, '350x600')

        f = Frame(self, bg = 'white')
        f.pack(fill=BOTH)
        for i in range(len(app.archives)):
            cmd = lambda e: print('hello')
            title = Label(f, text=app.archives[i].path, bg='white')
            title.bind('<Button-1>', cmd)
            title.grid(row = i, column = 0)
            localpath = Label(f, text=app.archives[i].localpath, bg='white')
            localpath.grid(row = i, column = 1)
            localpath.bind('<Button-1>', cmd)