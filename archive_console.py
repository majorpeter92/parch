from archive import Archive
from colorama import Fore, Back, Style
import re

class ArchiveConsole(Archive):
    def __init__(self, path, localpath):
        Archive.__init__(self, path, localpath)

    def listDirs(self):
        self.openDav()
        localdirs, remotedirs, localonly = Archive.listDirs(self)
        localonly.sort()
        remotedirs.sort()
        print('Archive contents:')

        dirs = []
        i = 1
        for dir in localonly:
            dirs.append(dir)
            print(Fore.RED+'  '+str(i)+') '+dir+' [local only]'+Fore.RESET)
            i += 1
        for dir in remotedirs:
            if dir in localdirs:
                dirs.append(dir)
                print(Fore.GREEN+'  '+str(i)+') '+dir+' [backed up]'+Fore.RESET)
                i += 1
            else:
                dirs.append(dir)
                print(Fore.YELLOW+'  '+str(i)+') '+dir+' [remote only]'+Fore.RESET)
                i += 1

        if len(localonly):
            print('  B) Backup all')
            print('  i-j) Backup range from i to j')

        i = input('Select item: ')
        if i == 'b' or i == 'B':
            for dir in localonly:
                print('\n\nBacking up "%s"' % dir)
                self.createBackup(dir)
            return True

        m = re.match(r"^(\d+)-(\d+)$", i)
        if m:
            for i in range(int(m.group(1))-1, int(m.group(2))):
                if i >= len(localonly):
                    break
                print('\n\nBacking up "%s"' % localonly[i])
                self.createBackup(localonly[i])
            return True

        try:
            i = int(i)-1
            dir = dirs[i]
        except:
            return False

        if dir in localonly:
            print('Creating backup...')
            self.createBackup(dir)
        elif not dir in localdirs:
            print('Download '+Fore.BLUE+'L'+Fore.RESET+'ocal copy or '+Fore.BLUE+'delete'+Fore.RESET+' remote?')
            answer = input('   > ')
            if answer == 'l' or answer == 'L':
                print('Downloading '+dir+'...')
                self.fetchBackup(dir)
            elif answer == 'delete':
                self.deleteRemote(dir)
        else:
            print('Delete '+Fore.BLUE+'L'+Fore.RESET+'ocal or delete '+Fore.BLUE+'R'+Fore.RESET+'emote? Or '+Fore.BLUE+'C'+Fore.RESET+'ancel?')
            answer = input('   > ')
            if answer == 'l' or answer == 'L':
                self.deleteLocal(dir)
            elif answer == 'r' or answer == 'R':
                self.deleteRemote(dir)

        print('\n\n')
        return True

    def uploadBackup(self, tmpFile):
        print('\n\nUploading encrypted file...')
        Archive.uploadBackup(self, tmpFile)