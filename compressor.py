"""
@author: Major Peter
7z interface for OS-independent usage
"""

import config
import os
from subprocess import Popen, PIPE
import subprocess

P7Z_OK_MSG = 'Everything is Ok'

def decrypt(fn, dest='.'):
    """
    decompress an encrypted 7z archive

    :param fn: input filename, *.7z
    :param dest: destination
    :return: true on success
    """
    # set up overwrite parameter
    ao = ''
    if len(config.P7Z_OVERWRITE):
        ao = ' -ao'+config.P7Z_OVERWRITE

    os.system('7z x "' + fn + '" "-o'+dest+'" -p'+config.PASSWORD + ao)

def encrypt(fn, dest):
    """
    compress a file or a directory to encrypted 7z archive
    :param fn: input file/dir
    :param dest: output *.7z
    :return:
    """
    if os.path.isfile(dest):
        os.remove(dest)     #delete current file if exists

    os.system('7za a "'+dest+'" "'+fn+'" -mx'+str(config.P7Z_COMPRESSION)+' -mhe -p'+config.PASSWORD)

    return True

