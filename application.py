from archive_console import ArchiveConsole
from davclient import DavClient
import pickle

_version = '0.0.2'

class Application:
    archives = []

    def __init__(self):
        self.loadArchives()

    def saveArchives(self):
        f = open('archives.pickle', 'wb')
        pickle.dump(self.archives, f)
        f.close()

    def loadArchives(self):
        try:
            f = open('archives.pickle', 'rb')
            self.archives = pickle.load(f)
            f.close()
        except:
            self.archives = []

    def createArchive(self, name, path):
        dav = DavClient()
        dav.mkArchive(name)

        self.archives.append(ArchiveConsole(name, path))
        self.saveArchives()

    def deleteArchive(self, n):
        self.archives.pop(n)
        self.saveArchives()
