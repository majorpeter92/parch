from pyDes import *
import os
import binascii
import tempfile
import config

key = des(config.FN_PASSWORD, CBC, '\0\0\0\0\0\0\0\0', pad=None, padmode=PAD_PKCS5)

def encode(data):
    data = data.encode().decode('utf-8')
    return binascii.hexlify(key.encrypt(data.encode())).decode('ascii')

def decode(data):
    hex = binascii.unhexlify(bytes(data, 'utf-8'))
    return key.decrypt(hex).decode('utf-8')

"""
@param databaseRoot root folder to store temporary 7z archive
"""
def getTemp7z(filename, databaseRoot):
    if os.name == 'posix':
        return databaseRoot+'/'+encode(filename)+'.7z'
    elif os.name == 'nt':
        return tempfile.gettempdir()+'\\'+encode(filename)+'.7z'
    raise ValueError('Unknown OS!');

