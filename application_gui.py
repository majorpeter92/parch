from application import Application, _version
from gui.archivelist import ArchiveList
import tkinter

class ApplicationGui(Application):
    def __init__(self):
        Application.__init__(self)
        ArchiveList(self).mainloop()

    def getTitle(self):
        return 'PArch GUI v' + _version

ApplicationGui()
