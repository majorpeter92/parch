== Requirements ==
python 3.4
pyDes 2.0.1 | fix basestring on py3 # basestring = (str, bytes)
easywebdav 1.2 / pythonwebdav
colorama 0.3.1

for gui:
install python3.4-tk package with apt-get on Ubuntu/Debian