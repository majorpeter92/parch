import config
import os
import easywebdav

class DavClient:
    dav = None
    baseurl = 'https://webdav.cubby.com:443/'
    basedir = '/Archive/'

    def __init__(self):
        self.dav = easywebdav.connect(
            host=config.DAV_HOST,
            username=config.DAV_USER,
            port=config.DAV_PORT,
            protocol=config.DAV_PROTOCOL,
            password=config.DAV_PASSWORD
        )

    def getBackupList(self, subdir):
        dir = self.dav.ls(self.basedir+subdir+'/')
        result = []
        for i in range(len(dir)):
            item = dir[i].name[len(self.baseurl)+len(self.basedir)+len(subdir):]
            if (len(item)):
                result.append(item)

        return result

    def upload(self, subdir, filename):
        if config.CURL_BIN:
            remoteUrl = config.DAV_PROTOCOL+'://'+config.DAV_HOST+self.basedir+subdir+'/'+os.path.basename(filename)

            if os.system(config.CURL_BIN + ' -u ' + config.DAV_USER+':'+config.DAV_PASSWORD + ' -T ' + filename + ' ' + remoteUrl) == 0:
                return True
        else:
            self.dav.upload(filename, self.basedir+subdir+'/'+os.path.basename(filename))
            return True # no response in easywebdav
        return False
        
    def download(self, remote_path, local_path):
        self.dav.download(self.basedir+remote_path, local_path)

    def deleteFile(self, path):
        self.dav.delete(self.basedir+path)

    def mkArchive(self, path):
        self.dav.mkdir(self.basedir+path)
