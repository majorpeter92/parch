import filenames
import compressor
from davclient import DavClient
import shutil
import os

class Archive():
    path = None
    localpath = None
    dav = None

    def __init__(self, path, localpath):
        self.path = path
        self.localpath = localpath

    def openDav(self): #TODO make it clever
        if not self.dav:
            self.dav = DavClient()

    def listDirs(self):
        localdirs = os.listdir(self.localpath)
        remotedirs_enc = self.dav.getBackupList(filenames.encode(self.path))
        remotedirs = []
        for dir in remotedirs_enc:
            remotedirs.append(filenames.decode(dir[:-3]))
        localonly = []
        for dir in localdirs:
            if not dir in remotedirs:
                localonly.append(dir)

        return localdirs, remotedirs, localonly

    def createBackup(self, dir):
        tmp = filenames.getTemp7z(dir, self.localpath)
        localdir = os.path.join(self.localpath, dir)
        compressor.encrypt(localdir, tmp)
        self.uploadBackup(tmp)

    def uploadBackup(self, tmpFile):
        self.dav.upload(filenames.encode(self.path), tmpFile)
        os.remove(tmpFile)

    def fetchBackup(self, dir):
        localpath = os.path.join(self.localpath, dir)+'.7z'
        self.dav.download(filenames.encode(self.path)+'/'+filenames.encode(dir)+'.7z', localpath)
        compressor.decrypt(localpath, self.localpath)
        os.remove(localpath)

    def deleteLocal(self, dir):
        shutil.rmtree(os.path.join(self.localpath, dir))

    def deleteRemote(self, dir):
        self.dav.deleteFile(filenames.encode(self.path)+'/'+filenames.encode(dir)+'.7z')