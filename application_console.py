from application import Application, _version
import colorama
from colorama import Fore, Back, Style

colorama.init()

class ApplicationConsole(Application):
    def __init__(self):
        Application.__init__(self)
        print('-= PArch v'+_version+' =-')

    def selectArchive(self):
        while True:
            print('Select archive:')
            for i in range(len(self.archives)):
                print('    '+str(i+1)+') '+self.archives[i].path)

            print('    C) create new...')
            if len(self.archives):
                print('    Dn) delete the nth archive')
            print('    X) exit')

            try:
                a = input('Number: ')
                if a == 'C' or a == 'c':
                    self.createArchive()
                    continue
                elif a[0] == 'D':
                    self.deleteArchive(int(a[1:])-1)
                    continue

                a = int(a)-1
            except:
                return False
            print('\n\n')

            while self.archives[a].listDirs():
                pass
            return True

    def createArchive(self):
        print('\n\n')
        print('Create new archive:')
        name = input('  Name: ')
        path = input('  Path: ')
        Application.createArchive(self, name, path)

    def deleteArchive(self, n):
        print('\n\n')
        print('Are you sure?')
        answer = input('  '+Fore.BLUE+'Y'+Fore.RESET+'es/'+Fore.BLUE+'N'+Fore.RESET+'o  > ')
        if answer == 'y' or answer == 'Y':
            Application.deleteArchive(self, n)
        print('\n\n')


    def loop(self):
        while self.selectArchive():
            pass

ApplicationConsole().loop()